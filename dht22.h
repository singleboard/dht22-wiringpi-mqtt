#ifndef DHT22_H
#define DHT22_H

struct hcfType {
    float h;
    float c;
    float f;
};

hcfType read_dht_data();

#endif // DHT22_H