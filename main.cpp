// systemctl status dht22.service
// g++ -Wall main.cpp dht22.cpp -o dht22 -lpaho-mqttpp3 -lpaho-mqtt3a -lwiringPi


#include "dht22.h"
#include <wiringPi.h>

#include <sstream>

#include <iostream>
#include <cstdlib>
#include <string>
#include <thread>	// For sleep
#include <atomic>
#include <chrono>
#include <cstring>

#include "mqtt/async_client.h"

using namespace std;

const std::string DFLT_SERVER_ADDRESS	{ "tcp://localhost:1883" };
const std::string DFLT_CLIENT_ID		{ "async_publish" };

const string TOPIC { "ANSWERPRO/DHT22/TEMP" };

const char* PAYLOAD1 = "Kukuddd!";
const char* PAYLOAD2 = "Hi there!";
const char* PAYLOAD3 = "Is anyone listening?";
const char* PAYLOAD4 = "Someone is always listening.";

const char* LWT_PAYLOAD = "Last will and testament.";

const int  QOS = 1;

const auto TIMEOUT = std::chrono::seconds(10);


/**
 * A callback class for use with the main MQTT client.
 */
class callback : public virtual mqtt::callback
{
public:
	void connection_lost(const string& cause) override {
		cout << "\nConnection lost" << endl;
		if (!cause.empty())
			cout << "\tcause: " << cause << endl;
	}

	void delivery_complete(mqtt::delivery_token_ptr tok) override {
		cout << "\tDelivery complete for token: "
			<< (tok ? tok->get_message_id() : -1) << endl;
	}
};

/////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    printf("DHT22 temperature/humidity test\n");

    if (wiringPiSetup() == -1)
        exit(1);

	string	address  = (argc > 1) ? string(argv[1]) : DFLT_SERVER_ADDRESS,
			clientID = (argc > 2) ? string(argv[2]) : DFLT_CLIENT_ID;

	cout << "Initializing for server '" << address << "'..." << endl;
	mqtt::async_client client(address, clientID);

	callback cb;
	client.set_callback(cb);

	mqtt::connect_options conopts;
	mqtt::message willmsg(TOPIC, LWT_PAYLOAD, 1, true);
	mqtt::will_options will(willmsg);
	conopts.set_will(will);

	cout << "  ...OK" << endl;

	try {
		cout << "\nConnecting..." << endl;
		mqtt::token_ptr conntok = client.connect(conopts);
		cout << "Waiting for the connection..." << endl;
		conntok->wait();
		cout << "  ...OK" << endl;

		// First use a message pointer.
		while(1) {

	        hcfType hcf = read_dht_data();
	        float temperature;
	        temperature = hcf.c;
	        if(temperature != 999) {
				cout << "Temperature..." << temperature << endl;
				std::ostringstream strs;
				strs << temperature;
				std::string str_temperature = strs.str();
				cout << str_temperature << endl;
				char dest[50] = "";
				strcat(dest, str_temperature.c_str());
				cout << "Sending message..."  << endl;
				mqtt::message_ptr pubmsg = mqtt::make_message(TOPIC, dest);
				pubmsg->set_qos(QOS);
				client.publish(pubmsg)->wait_for(TIMEOUT);
				cout << "  ...OK" << endl;
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));

	        }
        }

		// Disconnect
		cout << "\nDisconnecting..." << endl;
		conntok = client.disconnect();
		conntok->wait();
		cout << "  ...OK" << endl;
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return 1;
	}

 	return 0;
}

